# EDCI API Documentation

This directory contains descriptions of the APIs of each `app` ∈ {`wallet`,
`issuer`, `viewer`}. The files are in JSON format, so it is advisable to use
and editor that highlights JSON files to make them more readable.
