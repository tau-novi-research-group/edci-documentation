# Setting up Europass for local development

This document contains information on how to set up a local development
environment for Europass. The end result should look something like the
diagram in Figure 1.

![Figure 1: The architecture of Europass Digital Credentials Interoperability or EDCI.](images/edci-architecture/edci-architecture.svg "Figure 1: The architecture of Europass Digital Credentials Interoperability or EDCI.")

**Figure 1:** The architecture of Europass Digital Credentials Interoperability or EDCI.

In the future, instructions on setting up a production environment might be
included as well.

## Build instructions

Building the EDCI applications consists of the following phases:

1. installing the package managers NPM (comes with Node.js) and Maven, for
   JavaScript and Java respectively,

2. copying the contents of `europass-digital-credentials → configuration →
   documentation → mvn_settings.xml` into the file `$HOME → .m2 →
   settings.xml`,

3. installing the `.jar` files in `europass-digital-credentials →
   configuration → external_libs` into your local Maven repository `$HOME →
   .m2 → repository`, so that Maven can locate them,

4. disabling PKCA client authentication method and making other modifications
   to outdated source code,

5. possibly documenting out any malfunctioning tests that prevent Maven from
   compiling the project and

6. building the project with Maven and NPM, in that order.

Some of the issues mentioned here might have been fixed in later version of
EDCI. Version 1.4.1 seems to compile without issues on macOS Catalina. In any
case, more detailed instructions can be found below.

### Installing Node.js and Maven

For the Node.js runtime, it is recommended one uses the [Node Version
Manager][nvm] or `nvm` to install version 10.x of it. As for Maven, simply
download it from their [website][mvn-website], or use a version provided by the
package manager of your system. The latest version available via each
respective source should do nicely.

[nvm]: https://github.com/nvm-sh/nvm#node-version-manager---
[mvn-website]: https://maven.apache.org/download.cgi

### Copying Maven settings into the local Maven folder

Again, the copy operation `configuration/documentation/mvn_settings.xml` →
`$HOME/.m2/settings.xml` should be performed. The source file contains a list of
Maven repositories containing some of the dependencies of the project, in
addition to Maven build profiles needed in building the project.

Unless this is done, Maven will not know where to download a subset of the
dependencies from.

### Installing dependencies in this repository into the local Maven one

This is a fairly simple task. One simply needs to run the command
```sh
mvn install:install-file \
	-Dfile=configuration/external-libs/eui-angular2-servlet-1.0.0.jar \
	-DgroupId=eu.europa.ec.digit.uxatec.eui \
	-DartifactId=eui-angular2-servlet \
	-Dversion=1.0.0 \
	-Dpackaging=jar
```
to install the Angular 2 servlet, needed by the front-ends of the different
project components.

### Fixing broken source code

The method of authentication for the local Keycloak-based test environment is
slightly different from the one used in production, as the [PKCE extension] to
[OAuth2 Authorization Code Flow] mentioned in the above title is not needed. To
this end, commenting out the line ~62

	registeredClient.setCodeChallengeMethod(
		PKCEAlgorithm.parse(
			this.getString(
				EDCIConfig.OIDC_CODE_CHALLENGE_METHOD
			)
		)
	);

from the file

	edci-commons/src/main/java/eu/europa/ec/empl/edci/config/service/IConfigService.java

might be necessary to have each application work with Keycloak locally.

Also, some dependencies like the Liquibase library utilized by the Wallet
might be outdated. If strange exceptions arise in Catalina (Tomcat) logs
during application deployment, updating these in the `pom.xml` files of each
`app` ∈ {`viewer`, `wallet`, `issuer`} and making corresponding version
updates in any XML schema in `app → app-component → src → main → resources`
should get rid of these errors.

[PKCE extension]: https://oauth.net/2/pkce/
[OAuth2 Authorization Code Flow]: https://oauth.net/2/grant-types/authorization-code/

### Disabling malfunctioning tests (or fixing them)

During the build process, Maven might error out while complaining about certain
unit- and integration tests not passing. The naughty solution is to simply
comment the culprit tests out, the nice thing to do is to fix them.

### Building the EDCI applications

The Java and Angular (TypeScript) components need to be compiled separately.
The Angular components rely on the Java compilation having generated certain
[Swagger][swagger] files, meaning the Java components need to be compiled
first. This can be done by running

```sh
mvn clean install -e -P [profile from Maven settings.xml]
```

in the project root. Here `[profile from Maven settings.xml]` should be set to
`local-tomcat`.

[swagger]: https://swagger.io/

The front-ends of each of the EDCI applications `app` ∈ {`viewer`, `issuer`,
`wallet`} can be found in their respective sub-folders
europass-digital-credentials → `app` → `app`-web → src → main → angular, and
can be built by navigating to said folders and running

```sh
npm install --also=dev && npm run build-[profile from Maven settings.xml]
```

Note that `[profile from Maven settings.xml]` needs to be the same for both
the Java and Angular components. Again, a good one for local development is
`local-tomcat`.

### Other build notes

Certain unstable sun-prefixed classes might have been used during the
development of the project. If Maven complains about missing symbols and these
classes in the same context, the culprit classes need to be replaced with more
stable java-prefixed ones, that do the same job. See [here][sun-packages] for
details.

[sun-packages]: https://www.oracle.com/java/technologies/faq-sun-packages.html

## Setting up Keycloak as an authentication server

The intended way of using Europass is to have a OpenID Connect -based
authentication server handle the authentication. The easiest way to get this
done is to run such a server called *Keycloak* in a Docker container.

### Installing Keycloak

Install Docker as instructed on the [Docker installation
pages][docker-installation] and then run the following commands in the terminal
without the lines starting with `#`:

[docker-installation]: https://docs.docker.com/get-docker/

	# Create network for keycloak
	docker network create edci-network

	# First start up MySQL server…
	docker run \
		--name edci-keycloak-mysql \
		-d \
		--net edci-network \
		-e MYSQL_DATABASE=edci-keycloak \
		-e MYSQL_USER=edci-keycloak \
		-e MYSQL_PASSWORD=password \
		-v /path/to/local-database:/var/lib/mysql \
		-e MYSQL_ROOT_PASSWORD=root_password \
		mysql

	# … then run Keycloak with token exchange enabled.
	docker run \
		--name edci-keycloak \
		-d \
		-p 9000:8080 \
		--net edci-network \
		-e KEYCLOAK_USER=admin \
		-e KEYCLOAK_PASSWORD=admin \
		-e DB_VENDOR=mysql \
		-e DB_ADDR=edci-keycloak-mysql \
		-e DB_DATABASE=edci-keycloak \
		-e DB_USER=edci-keycloak \
		-e DB_PASSWORD=password \
		-e JAVA_OPTS_APPEND="
			-Dkeycloak.profile.feature.token_exchange=enabled
			-Dkeycloak.profile.feature.admin_fine_grained_authz=enabled
		" \
		quay.io/keycloak/keycloak:15.0.2

After this, you should have a Keycloak authentication server and the MySQL
database it utilizes running as a background service. You can connect to
Keycloak by navigating to http://localhost:9000 in a web browser of your
choice.

If you wish to stop the containers, run

	docker stop edci-keycloak && docker stop edci-mysql-keycloak

on the command line. Run

	docker start edci-keycloak && docker start edci-mysql-keycloak

to restart the containers with the settings specified during their creation.

### Configuring Keycloak

There are 3 things to be done here for each of the apps:

1. create an authentication realm for the EDCI apps to connect to,
2. create a test user in the realm that has access to each app.
3. create a client for each of the EDCI apps Viewer, Issuer and/or Wallet you
   wish to use.

All of these can be configured by logging in to the admin console in keycloak
at localhost:9000 and using the provided GUI. The user name and password are
"admin" and "admin", as specified in the above Docker commands.


#### Creating authentication realm

This is very simple. Just follow the [realm creation instructions] in the
Keycloak documentation.

[realm creation instructions]: https://www.keycloak.org/docs/latest/getting_started/index.html#creating-a-realm


#### Creating a user in a realm

Again, simply follow the [user creation instructions] in the Keycloak
documentation. A password for a user should be set under Users → user →
credentials.

**Note:** the created user *must* be given an actual name in addition to their
username. If this is not done, the applications will look as if nobody had
logged in, even if a log-in was successful.

[user creation instructions]: https://www.keycloak.org/docs/latest/getting_started/index.html#creating-a-user


#### Adding the applications as clients

The process is explained in the [registering a sample application] portion of
the Keycloak documentation. The only difference is that naming of the clients
should probably follow the form `edci-app`, where `app` ∈ {`viewer`, `issuer`,
`wallet`}.

[registering a sample application]: https://www.keycloak.org/docs/latest/getting_started/index.html#registering-the-wildfly-application

After the client `app` has been created, these client settings are required:

1. Settings → Client protocol: openid-connect
2. Settings → Access type: confidential
3. Settings → Root URL: http://localhost:8080/europass2/edci-app/
4. Credentials → Client authenticator: Client Id and secret
5. Credentials → Generate a secret

The root URL assumes that each Tomcat application `app` ∈ {`viewer`, `issuer`,
`wallet`} is running at port 8080. The `secret` generated in step 5 should be
placed into the application runtime configuration file in tomcat → conf → edci
→ app → app.security as the property oidc.client.secret=`secret`.


#### Additional steps for the Wallet

The Wallet also requires Keycloak to run with token exchange enabled and
configured for the Wallet itself. The above Docker run command already does the
enabling, but for the actual configuration see [Granting Permission for the
Exchange][keycloak-exchange-grant] and [Token
Exchange][keycloak-token-exchange] in the Keycloak documentation.

The Wallet also needs to have its own administrator `wallet-admin` set up with
a password. Just create another user with this user-name and name them Wallet
Admin in the Keycloak user creation console.

[keycloak-exchange-grant]: https://www.keycloak.org/docs/latest/securing_apps/#_client_to_client_permission
[keycloak-token-exchange]: https://github.com/keycloak/keycloak-documentation/blob/main/securing_apps/topics/token-exchange/token-exchange.adoc


## Deploying the applications on Tomcat

This consists of two steps per application, in addition to of course installing
Tomcat:

1. add application runtime configuration files to Tomcat configuration folder
   for each application and
2. Copying the WAR files generated by Maven during project compilation to the
   Tomcat webapps/ folder

**Note:** It might also be necessary to place the *Keycloak Tomcat adapters*
([download][keycloak adapters]) into `tomcat → lib` to have Tomcat be able to
communicate with Keycloak.

[keycloak adapters]: https://www.keycloak.org/downloads

### Installing Tomcat

The easiest thing to do is to install Tomcat via Docker. That way it is not
necessary to fool around with `systemd` or other such daemon managers, as
Docker functions as one.

Annoyingly, the Tomcat configuration files are not available as standalone
downloads, so one has to download a version of Tomcat, preferrably [version ≥
9][tomcatv9], that they wish to use and use the configuration files that come
with it in Docker. If the Tomcat installation is saved under `$HOME → tomcat →
9.0.55`, the command to start up a Tomcat container with an embedded JRE8 and
with the downloaded Tomcat config files is

	docker run \
		--name edci-tomcat \
		--net edci-network \
		-d \
		-e CATALINA_BASE='/usr/local/tomcat' \
		-p 8080:8080 \
		-v $HOME/tomcat/9.0.55/conf:/usr/local/tomcat/conf \
		-v $HOME/tomcat/9.0.55/conf/edci:/usr/local/tomcat/lib-client/classpathFILE/edci \
		-v $HOME/tomcat/9.0.55/lib:/usr/local/tomcat/lib \
		-v $HOME/tomcat/9.0.55/logs:/usr/local/tomcat/logs \
		-v $HOME/tomcat/9.0.55/webapps:/usr/local/tomcat/webapps \
		-v $HOME/edci.credentials:/usr/local/tomcat/temp/credentials \
		tomcat:9.0.55-jre8-openjdk-buster

where the directories mounted via the `-v` flags are based on the default
internal setup of the Tomcat Docker container:

	CATALINA_BASE:   /usr/local/tomcat
	CATALINA_HOME:   /usr/local/tomcat
	CATALINA_TMPDIR: /usr/local/tomcat/temp
	JRE_HOME:        /usr
	CLASSPATH:       /usr/local/tomcat/bin/bootstrap.jar:/usr/local/tomcat/bin/tomcat-juli.jar

To use the Tomcat Manager application the file `$HOME → tomcat → 9.0.55 → conf
→ tomcat-users.xml` has to be edited with the following contents:

	<role rolename="admin-gui"/>
	<role rolename="manager-gui"/>
	<user username="admin" password="admin" roles="admin-gui,manager-gui"/>

and the file `$HOME → tomcat → 9.0.55 → webapps → manager → META-INF →
context.xml` should look as follows:

	<Context antiResourceLocking="false" privileged="true" →
	  <CookieProcessor className="org.apache.tomcat.util.http.Rfc6265CookieProcessor"
		  sameSiteCookies="strict" />
		<!--
		<Valve className="org.apache.catalina.valves.RemoteAddrValve"
			allow="127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" />
		-->
	  <Manager sessionAttributeValueClassNameFilter="java\.lang\.(?:Boolean|Integer|Long|Number|String)|org\.apache\.catalina\.filters\.CsrfPreventionFilter\$LruCache(?:\$1)?|java\.util\.(?:Linked)?HashMap"/>
	</Context>

Note that the username and password of the admin user are only for local
development. They should be made more secure if the application is deployed to
the world outside the development computer.

[tomcatv9]: https://tomcat.apache.org/download-90.cgi

### Required external libraries

The EDCI applications require certain Java libraries installed
in the `tomcat → lib` folder to function. These need to be downloaded
off the internet:

* apache-log4j-extras-1.2.17.jar,
  <https://downloads.apache.org/logging/log4j/extras/1.2.17/>
* bcprov-jdk15on-1.68.jar,
  <https://mvnrepository.com/artifact/org.bouncycastle/bcprov-jdk15on/1.68>
* javax.mail-1.5.0.jar,
  <https://mvnrepository.com/artifact/com.sun.mail/javax.mail/1.5.0>
* jstl-1.2.jar, <https://mvnrepository.com/artifact/javax.servlet/jstl/1.2>
* log4j-1.2.17.jar,
  <https://mvnrepository.com/artifact/log4j/log4j/1.2.17>
* javax.ws.rs-api-2.1.1.jar:
  <https://repo1.maven.org/maven2/javax/ws/rs/javax.ws.rs-api/2.1.1/>.
* mysql-connector-8.0.27:
  <https://repo1.maven.org/maven2/mysql/mysql-connector-java/8.0.27/>

The applications will not function if these are not in place.

### Creating databases for the Wallet and the Issuer

Both of the mentioned applications need their *own* databases to function. The
least resource intensive way to achieve this would be to start up to start up
a single MySQL Docker container, which would then house both of these.
However, that would take some amount of scripting to achieve, and hence two
separate Docker containers can also be started, each housing one database:

	docker run \
		--name edci-wallet-mysql \
		--net edci-network \
		-p 3306:3306 \
		-d \
		-e MYSQL_DATABASE=edci_wallet \
		-e MYSQL_USER=edci \
		-e MYSQL_PASSWORD=password \
		-v $HOME/databases/edci-wallet:/var/lib/mysql \
		-e MYSQL_ROOT_PASSWORD=password \
		mysql

	docker run \
		--name edci-issuer-mysql \
		--net edci-network \
		-p 3307:3306 \
		-d \
		-e MYSQL_DATABASE=edci_issuer \
		-e MYSQL_USER=edci \
		-e MYSQL_PASSWORD=password \
		-v $HOME/databases/edci-issuer:/var/lib/mysql \
		-e MYSQL_ROOT_PASSWORD=password \
		mysql

Notice that the environment variables `MYSQL_{DATABASE,USER,PASSWORD}` must
match those given in the issuer.properties and wallet.properties configuration
files specified below.

### Adding runtime configuration files and dependecies for each EDCI application

Each EDCI application `app` ∈ {`wallet`, `issuer`, `viewer`} needs to have
their Tomcat configuration files placed in the folder *tomcat → conf → edci →
app*. Skeletons for these files for each app can be found in
*europass-digital-credentials → app → app-web → src → main → resources →
config → ext*, in the project version control repository. Many of the
contained values are self-explanatory or just not required in a local setup,
but some of them require either getting a value from the Keycloak console or
setting up an email address for the Wallet to communicate with.

**Note:** any property values of the form

	property=${SOME_CAPITALIZED_VARIABLE_NAME}

need to be set by *you*, the person setting up the EDCI apps. These usually
correspond to values set up in Keycloak. They might also be set up as
environment variables inside the Tomcat container running the EDCI apps, if
one so desires, via the

	-e name=value

switches in the Tomcat container creation command.


#### Viewer configuration files and dependencies

There are 3 relevant configuration files. These need to be placed in the
folder tomcat → conf → edci → viewer.


##### security.properties

	#SESSION PROPERTIES
	app.session.timeout=10
	expired.session.url=http://localhost:8080/europass2/edci-viewer
	session.expired.redirect.url=/home
	#OIDC PROPERTIES
	oidc.client.id=edci-viewer
	oidc.client.secret=${EUROPASS_VIEWER_SECRET}
	oidc.scopes=openid,email,profile
	oidc.endpoint.auth.method=SECRET_BASIC
	oidc.signing.alg=RS256
	# oidc.code.challenge.method=S256 ← This is not used in local environment
	oidc.invalid.session.url=/sessionExpired
	oidc.mock.user.active=false
	oidc.wallet.client.id=edci-wallet
	oidc.use.token.exchange=keycloak
	#OIDC URLS PROPERTIES
	oidc.jwk.url=http://localhost:9000/certs
	oidc.login.url=/auth/oidc/eulogin
	oidc.logout.url=${oidc.login.url}/logout
	oidc.post.logout.url=http://localhost:8080/europass2/edci-viewer
	oidc.logout.success.url=/
	oidc.idp.url=http://localhost:9000/auth/realms/edci
	oidc.idp.end.session.url=http://localhost:9000/auth/realms/edci/protocol/openid-connect/logout
	oidc.auth.request.url=http://localhost:9000/auth/realms/edci/protocol/openid-connect/auth
	oidc.idp.introspection.url=http://localhost:9000/auth/realms/edci/protocol/openid-connect/token/introspect
	oidc.redirect.url=http://localhost:8080/europass2/edci-viewer/auth/oidc/eulogin
	oidc.success.default.url=http://localhost:8080/europass2/edci-viewer
	oidc.anonymous.pattern=/.*
	#OIDC MOCK USER PROPERTIES
	oidc.mock.user.info={"sub":"mockuser","email":"mockuser@everis.com","email_verified":true,"name":"Mock","nickname":"MockU","password":"password","groups":["Everyone", "issuer", "viewer"]}
	# MacOS-specific settings
	# oidc.idp.url=http://kubernetes.docker.internal:9000/auth/realms/edci
	# oidc.idp.end.session.url=http://kubernetes.docker.internal:9000/auth/realms/edci/protocol/openid-connect/logout
	# oidc.auth.request.url=http://kubernetes.docker.internal:9000/auth/realms/edci/protocol/openid-connect/auth
	# oidc.idp.introspection.url=http://kubernetes.docker.internal:9000/auth/realms/edci/protocol/openid-connect/token/introspect
	# oidc.redirect.url=http://localhost:8080/europass2/edci-viewer/auth/oidc/eulogin
	# oidc.success.default.url=http://localhost:8080/europass2/edci-viewer/


The security property `oidc.client.secret` has to be generated by Keycloak for
each application and copied to the above file afterwards. This file also
assumes, that Tomcat is running at http://localhost:8080 and the Keycloak
Docker container has been set up to forward its port 8080 to the host port
9000, as in Keycloak is running at http://localhost:9000.

##### viewer.properties

	#ENVIRONMENT PROPERTIES
	server.full.address=http://localhost:8080
	app.context.root=/europass2/edci-viewer
	#ROOT CONTEXT PROPERTIES
	app.swagger.ui.context.root=/europass2/edci-viewer-swaggerUI
	#EXTERNAL RESOURCES PROPERTIES
	wallet.url=http://localhost:8080/europass2/edci-wallet
	#EXTERNAL RESOURCES PROPERTIES for Europass v1.3
	wallet.verify.xml.url=${wallet.url}/api/v1/wallets/credentials/verifyXML
	wallet.verify.id.url=${wallet.url}/api/v1/wallets/_userId/credentials/_uuid/verify
	wallet.download.xml.url=${wallet.url}/api/v1/wallets/_userId/credentials/_uuid
	wallet.download.verifiable.presentation.url=${wallet.url}/api/v1/wallets/_userId/credentials/verifiable
	wallet.download.verifiable.from.file.presentation.url=${wallet.url}/api/v1/wallets/credentials/presentation
	wallet.download.shared.verifiable.presentation.url=${wallet.url}/api/v1/sharelinks/_shareHash/presentation
	wallet.download.shared.verification.url=${wallet.url}/api/v1/sharelinks/_shareHash/verify
	wallet.download.shared.xml.url=${wallet.url}/api/v1/sharelinks/_shareHash/credentials
	wallet.get.sharelink.fetch.url=${wallet.url}/api/v1/sharelinks/_shareHash
	wallet.sharelink.create.url=${wallet.url}/api/v1/wallets/_userId/credentials/_uuid/sharelink


##### viewer\_front.properties

	production=false
	serviceWorker=false
	hasLabelsOnly=true
	apiBaseUrl=http://localhost:8080/europass2/edci-viewer/api
	issuerBaseUrl=http://localhost:8080/europass2/edci-issuer
	walletBaseUrl=http://localhost:8080/europass2/edci-wallet
	europassRoot=https://webgate.acceptance.ec.europa.eu/europass
	ePortfolioUrl=https://webgate.acceptance.ec.europa.eu/europass/eportfolio/api/europass-auth/authenticate?redirect_uri=https://webgate.acceptance.ec.europa.eu/d/empl/europass/
	downloadCredentialUrl=/v1/credentials/{walletAddress}/verifiable
	walletAddressParameter={walletAddress}
	downloadSharedCredentialUrl=/v1/sharelinks/{shareHash}/presentation
	shareHashParameter={shareHash}
	loginUrl=/auth/oidc/eulogin
	logoutUrl=/auth/oidc/eulogin/logout
	isMockUser=true
	csrfEnabled=false
	hasBranding=true
	headerImagePath=assets/images/logo_countries/
	homeMainTitle=home.main-info.title
	homeMainDescription=home.main-info.description
	homeMenuMainTitle=home.credentialsForCitizens
	homeMenuDescription=home.menu.description
	viewerBaseUrl=http://localhost:8080/europass2/edci-viewer

Again, note that the localhost ports should be the ones where Tomcat has
deployed each application.


#### Wallet configuration files and setup

There are 5 configuration files to add. These are to be placed in tomcat →
conf → edci → wallet. In addition to that, the Wallet requires its own e-mail
account set up at GMail or other such provider, and a database of its own.

##### mail.properties

	#DEBUG PROPERTIES
	mail.debug=True
	#SMTP SERVER PROPERTIES
	mail.smtp.user=${EUROPASS_WALLET_EMAIL} # tuni.europass.test@gmail.com
	mail.smtp.pass=${EDCI_WALLET_EMAIL_PASSWORD}
	mail.smtp.host=smtp.gmail.com
	mail.smtp.auth=true
	#TLS PROPERTIES
	mail.smtp.starttls.enable=true
	#If empty, default subject is "Your <<Credential Title>>"
	mail.subject=A [$credentialName$] has been added to your Europass wallet.


##### proxy.properties

Setting the proxy to false disables it for local development. These should only
be needed when actually deploying the application.

	#HTTP PROXY
	proxy.http.enabled=false
	proxy.http.host=XXXXXX
	proxy.http.port=XXXXXX
	proxy.http.user=XXXXXX
	proxy.http.pwd=XXXXXX
	#HTTPS PROXY
	proxy.https.enabled=false
	proxy.https.host=XXXXXX
	proxy.https.port=XXXXXX
	proxy.https.user=XXXXXX
	proxy.https.pwd=XXXXXX


##### security.properties

	#BASIC AUTH PROPERTIES
	basic.auth.pattern=/.*/bulk
	basic.user=${EUROPASS_WALLET_ADMIN_USERNAME}
	basic.password=${EUROPASS_WALLET_ADMIN_PASSWORD}
	#OIDC URLS PROPERTIES
	oidc.idp.url=http://localhost:9000
	# MacOS Docker: oidc.idp.url=http://kubernetes.docker.internal:9000
	oidc.idp.introspection.url=${oidc.idp.url}/token/introspection
	#OIDC PROPERTIES
	oidc.client.id=${EUROPASS_WALLET_ID}
	oidc.client.secret=${EUROPASS_WALLET_SECRET}
	oidc.use.token.exchange=false
	oidc.scopes=openid,email,profile
	oidc.endpoint.auth.method=SECRET_BASIC
	oidc.code.challenge.method=S256
	oidc.signing.alg=RS256
	oidc.invalid.session.url=/sessionExpired
	#OIDC MOCK USER PROPERTIES
	oidc.mock.user.active=false
	oidc.mock.user.info={"sub":"mockuser","email":"mockuser@everis.com","email_verified":true,"name":"Mock","nickname":"MockU","password":"password","groups":["Everyone", "issuer", "viewer"]}


##### wallet.properties

	#ROOT CONTEXT PROPERTIES
	wallet.url=https://localhost:8080/europass2/edci-wallet
	#EXTERNAL RESOURCES PROPERTIES
	viewer.url=http://localhost:8080/europass2/edci-viewer
	publications.rdf.sparql.endpoint=http://publications.europa.eu/webapi/rdf/sparql
	pdf.download.url=https://webgate.acceptance.ec.europa.eu/europass/eportfolio/api/office/generate/pdf
	png.download.url=https://dgempl-single-portal-demo-1.arhs-developments.com/europass/eportfolio/api/office/generate/png
	europass.url=http://europass.eu
	#WALLET PROPERTIES
	wallet.address.prefix=europass.eu
	wallet.create.sending.credential=true
	wallets.clean.old.days=1
	wallets.clean.old.with.no.cred=true
	allow.unsigned.credentials=true
	store.diploma.database=true
	#CORS WHITELIST
	allowed.domains=localhost,http://localhost:4200
	#DB DATASOURCE
	datasource.db.driverClassName=com.mysql.cj.jdbc.Driver
	datasource.db.url=jdbc:mysql://{kubernetes.docker.internal ∨ localhost}:3306/edci_wallet
	datasource.db.username=edci
	datasource.db.password=password
	datasource.db.ddl-generation=create-or-extend-tables
	datasource.db.target-database=MySQL
	#SMTP SERVER PROPERTIES
	mail.user=${EUROPASS_EMAIL_USER}
	mail.pass=${EUROPASS_EMAIL_PASSWORD}
	mail.host=smtp.gmail.com
	mail.port=587
	mail.auth=true
	#TLS PROPERTIES
	mail.tls.protocols=TLSv1.2
	mail.starttls.enable=true
	mail.transport.protocol=smtps
	#If empty, default subject is "Your <<Credential Title>>"
	mail.subject=A [$credentialName$] has been added to your Europass wallet.
	#XML Signature
	signature.xml.digestAlgorithm=SHA256
	signature.xml.level=XAdES-BASELINE-B
	signature.xml.packaging=ENVELOPED
	signature.xml.validate.revogation=true
	signature.xml.tsp_server=https://freetsa.org/tsr
	#PDF Signature
	signature.pdf.digestAlgorithm=SHA256
	signature.pdf.level=PAdES-BASELINE-B
	signature.pdf.packaging=ENVELOPED
	signature.pdf.validate.revogation=true
	signature.pdf.tsp_server=https://freetsa.org/tsr


##### wallet\_dss.properties

	# XML Signature
	signature.xml.digestAlgorithm=SHA256
	signature.xml.level=XAdES-BASELINE-B
	signature.xml.packaging=ENVELOPED
	signature.xml.validate.revogation=true
	signature.xml.tsp_server=XXXXXX
	# PDF Signature
	signature.pdf.digestAlgorithm=SHA256
	signature.pdf.level=XAdES-BASELINE-B
	signature.pdf.packaging=ENVELOPED
	signature.pdf.validate.revogation=true
	signature.pdf.tsp_server=XXXXXX

##### wallet\_front.properties

	production=false
	enableDevToolRedux=true

##### Setting up the Wallet database and email

For the e-mail address, simply create a GMail account for the Wallet. Then we
need to configure Tomcat itself, in addition to starting up a MySQL Docker
container. Tomcat itself is configured by adding the resources (in v1.3)

	<Resource
		name="mail/Session"
		auth="Container"
		type="javax.mail.Session"
		mail.transport.protocol="smtp"
		mail.smtp.host="smtp.gmail.com"
		mail.smtp.port="587"
		mail.smtp.user="${EUROPASS_WALLET_EMAIL}"
		mail.smtp.starttls.enable="true"
		mail.smtp.auth="true"
		password="${EUROPASS_WALLET_EMAIL_PASSWORD}"
	/>

and

	<Resource
		name="jdbc/mare"
		auth="Container"
		type="javax.sql.DataSource"
		driverClassName="com.mysql.cj.jdbc.Driver"
		url="jdbc:mysql://localhost:3306/edci-wallet"
		<!-- For MacOS + Docker
		url="jdbc:mysql://kubernetes.docker.internal:3306/edci-wallet"
		-->
		username="${EUROPASS_WALLET_ADMIN_USERNAME}"
		password="${EUROPASS_WALLET_ADMIN_PASSWORD}"
		maxTotal="20"
		maxIdle="10"
		maxWaitMillis="-1"
	/>

to `tomcat → conf → context.xml`, and adding the [MySQL
connector][mysql-connector] with the same version as in the MySQL Docker
container to `tomcat → lib`. Once the above Tomcat configuration is in
place, we can generate a MySQL Docker container, similarly to how we created
one for Keycloak:

[mysql-connector]: https://repo1.maven.org/maven2/mysql/mysql-connector-java/8.0.27/

	docker run \
		--name edci-mysql \
		--net edci-network \
		-p 3306:3306 \
		-d \
		-e MYSQL_DATABASE=edci-wallet \
		-e MYSQL_USER=${EUROPASS_DATABASE_USERNAME} \
		-e MYSQL_PASSWORD=${EUROPASS_DATABASE_PASSWORD} \
		-v ${EUROPASS_DATABASE_FOLDER}:/var/lib/mysql \
		-e MYSQL_ROOT_PASSWORD=${EUROPASS_DATABASE_ROOT_PASSWORD} \
		mysql

The database- and usernames, passwords and port numbers should match those
specified in the app-specific app.properties files. After the database is
running and Tomcat is restarted, things should be working as intended.

**Note:** Oracle database was used in developing Europass, so some of the
queries in the source code might need to be translated to a different SQL
dialect, if that dialect is to be used. In order to use MySQL, the query on
line ~12 of the file `edci-dss-utils → src → main → java → eu → europa → ec →
empl → edci → dss → config → EdciJdbcCacheCRLSource.java` has to be translated
to MySQL: the `NUMBER(N) = NUMBER(N, 0)` data type supported by Oracle DB is
not supprorted by MySQL and has to be changed to `DECIMAL(N) = DECIMAL(N, 0)`.
There might also be other files that need changing for the server to start up.


#### Issuer configuration files and setup

The issuer is similar in structure to the Wallet and requires similar
configuration files. These are to be placed in tomcat → conf → edci → issuer.
It also requires a database and an email address, but the same e-mail used for
the Wallet should do for the Issuer as well.

##### issuer.properties

	#WALLET FEATURES PROPERTIES
	wallet.send.by.email.enabled=true
	#PERSISTENCE PROPERTIES
	eclipselink.ddl-generation=create-or-extend-tables
	#ENVIRONMENT PROPERTIES
	active.profile=LOCAL
	server.full.address=http://localhost:8080
	#ROOT CONTEXT PROPERTIES
	app.context.root=/europass2/edci-issuer
	app.swagger.ui.context.root=/europass2/edci-issuer-swaggerUI
	#EXTERNAL RESOURCES PROPERTIES
	viewer.url=http://localhost:8080/europass2/edci-viewer
	wallet.url=http://wallet:8080/europass2/edci-wallet
	publications.rdf.sparql.endpoint=http://publications.europa.eu/webapi/rdf/sparql
	png.download.url=https://dgempl-single-portal-demo-1.arhs-developments.com/europass/eportfolio/api/office/generate/png
	#API PROPERTIES
	api.base.path=/api
	#FILESYSTEM AND TEMPORAL PROPERTIES
	tmp.data.location=${catalina.home}/temp/
	tmp.data.credential.folder=credentials/
	tmp.data.public.credential.folder=/credentials_public
	#ACTIVATE(CONSOLE_APPENDER)/DEACTIVATE(NULL_APPENDER) CONSOLE APPENDER.
	console.appender.ref=CONSOLE_APPENDER
	#DB DATASOURCE
	datasource.db.driverClassName=com.mysql.cj.jdbc.Driver
	datasource.db.url=jdbc:mysql://kubernetes.docker.internal:3307/edci_issuer
	datasource.db.username=edci
	datasource.db.password=password
	datasource.db.ddl-generation=create-or-extend-tables
	datasource.db.target-database=MySQL
	#SMTP SERVER PROPERTIES
	mail.user=${EUROPASS_EMAIL_ADDRESS}
	mail.pass=${EUROPASS_EMAIL_PASSWORD}
	mail.host=smtp.gmail.com
	mail.port=587
	mail.auth=true
	mail.from=
	#TLS PROPERTIES
	mail.tls.protocols=TLSv1.2
	mail.starttls.enable=true
	mail.transport.protocol=smtps
	#If empty, default subject is "Your <<Credential Title>>"
	mail.subject=
	#DSS SEALING PROPERTIES
	allow.qseals.only=false
	#DSS TIMESTAMP PROPERTIES
	dss.tsp=http://dss.nowina.lu/pki-factory/tsa/good-tsa
	dss.cert.path=${catalina.home}/conf/edci/issuer/cert/QSEALC.pfx
	sealing.api.send.temporal=false
	max.consecutive.errors.batch.sealing=


##### issuer\_front.properties

	production=false
	hasLabelsOnly=true
	apiBaseUrl=http://localhost:8080/europass2/edci-issuer/api
	viewerBaseUrl=http://localhost:8080/europass2/edci-viewer
	loginUrl=/auth/oidc/eulogin
	logoutUrl=/auth/oidc/eulogin/logout
	europassRoot=https://webgate.acceptance.ec.europa.eu/europass
	csrfEnabled=false
	hasBranding=true
	headerImagePath=assets/images/logo_countries/
	homeMainTitle=home.main-info.title
	homeMainDescription=home.main-info.description
	concentText=credential-builder.concent
	homeCredentialsForIssuersTitle=home.menu-links.credentials-for-issuers.title
	homeCredentialsForIssuersDescription=home.menu-links.credentials-for-issuers.content.description-1
	homeCredentialsForIssuersDescriptionWithLink=home.menu-links.credentials-for-issuers.content.description-2
	isMockUser=false
	issuerBaseUrl=http://localhost:8080/europass2/edci-issuer
	enabledLocalSealing=true


##### proxy.properties

Again, the proxy server is not needed if the Issuer is being deployed for
local testing.

	#HTTP PROXY
	proxy.http.enabled=false
	proxy.http.host=158.169.9.13
	proxy.http.port=8012
	proxy.http.user=${EUROPASS_PROXY_USER}
	proxy.http.pwd=${EUROPASS_PROXY_PASSWORD}

	#HTTPS PROXY
	proxy.https.enabled=false
	proxy.https.host=158.169.9.13
	proxy.https.port=8012
	proxy.http.user=${EUROPASS_PROXY_USER}
	proxy.http.pwd=${EUROPASS_PROXY_PASSWORD}


##### security.properties

	#SESSION PROPERTIES
	app.session.timeout=60
	expired.session.url=${server.full.address}${app.context.root}
	session.expired.redirect.url=/screen/create/prepare
	#OIDC PROPERTIES
	oidc.client.id=edci-issuer
	oidc.client.secret=${EUROPASS_ISSUER_SECRET}
	oidc.scopes=openid,email,profile
	oidc.endpoint.auth.method=SECRET_BASIC
	oidc.signing.alg=RS256
	oidc.invalid.session.url=/sessionExpired
	oidc.use.token.exchange=false
	#OIDC URLS PROPERTIES
	oidc.jwk.url=http://kubernetes.docker.internal:9000/certs
	oidc.login.url=/auth/oidc/eulogin
	oidc.logout.url=/auth/oidc/eulogin/logout
	oidc.post.logout.url=http://localhost:8080/europass2/edci-issuer
	oidc.logout.success.url=/
	oidc.idp.url=http://kubernetes.docker.internal:9000/auth/realms/edci
	oidc.idp.end.session.url=http://kubernetes.docker.internal:9000/auth/realms/edci/protocol/openid-connect/logout
	oidc.auth.request.url=http://kubernetes.docker.internal:9000/auth/realms/edci/protocol/openid-connect/auth
	oidc.idp.introspection.url=http://kubernetes.docker.internal:9000/auth/realms/edci/protocol/openid-connect/token/introspect
	oidc.redirect.url=http://localhost:8080/europass2/edci-issuer/auth/oidc/eulogin
	oidc.success.default.url=http://localhost:8080/europass2/edci-issuer/#/credential-builder
	oidc.secured.pattern=.*/specs(/.*|$|\\?.*)
	oidc.anonymous.pattern=/.*
	#OIDC MOCK USER PROPERTIES
	oidc.mock.user.active=false
	oidc.mock.user.info={"sub":"mockuser","email":"mockuser@everis.com","email_verified":true,"name":"Mock","nickname":"MockU","password":"password","groups":["Everyone", "issuer", "viewer"]}
	#BASIC AUTH PROPERTIES
	basic.auth.pattern=/.*/public/.*
	admin.user=${EUROPASS_ADMIN_USER}
	admin.password=${EUROPASS_ADMIN_USER_PASSWORD}


##### users.properties

	admin=[password],ROLE_ADMIN,enabled


### Deploying each application

If there were no issues during the compilation of the project and the above
per-`app` configuration files have been placed in the Tomcat conf/edci/`app`
folders, it should be enough to copy each of the generated WAR files to the
folder

	$CATALINA_HOME/webapps/

Tomcat should automatically unpack these and the applications shoud become
visible in the Tomcat manager application, if it has been installed.

If this does not happen automatically, the WAR files need to be unpacked
manually, or Tomcat has to be configured to perform the automatic unpacking in
its own configuration file `$CATALINA_HOME/conf/server.xml`.

Once the applications `app` ∈ {`viewer`, `wallet`, `issuer`} have been deployed and
running, they should be located at http://localhost:8080/europass2/edci-app.
