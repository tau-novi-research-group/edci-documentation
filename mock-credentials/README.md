# Mock credentials

This diretory contains test credentials created with the local development
version of EDCI Issuer. Note that they are most likely unsigned, as as of this
writing they have been extracted by mounting a folder inside the EDCI Issuer
container and copying them here before the signing phase. This was necessary,
because version 1.4.1 of EDCI Issuer did not implement local signing.
