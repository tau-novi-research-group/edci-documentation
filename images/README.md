# Images

Contains images used in the other documents of this project. These can be
referenced by placing a

	![Alternative text.](path/to/image.svg "Title text.")

in a GitLab Markdown file, at the position where the images are to be placed.
Some of the folders also contain $`LaTeX`$ sources used to generate the
images. Conversion has been done with the toolchain

	source.tex -(pdflatex)→ PDF -(pdf2svg)→ SVG .

The PDF files are omitted, as they are binary blobs ill-suited for version
control and cannot be imported on web pages.
