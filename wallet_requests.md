# EDCI Wallet requests

This document contains cURL commands for communicating with the EDCI Wallet.
In order for them to work as-is, certain environment variables will need to be
set in the runtime configuration file of your shell of choice. To this end the
following lines should be added to the file `.bashrc`, `.zshrc` or `.fishrc`
in the $HOME directory of your user account:

	export EUROPASS_USER_EMAIL="your.email@domain.suffix from Keycloak"
	export EUROPASS_USERNAME="your username from Keycloak"
	export EUROPASS_PASSWORD="your password from Keycloak"
	export EUROPASS_USER_ID="your user id from Keycloak"
	export EUROPASS_WALLET_SECRET="wallet secret from Keycloak"

The values of these variables should correspond to the ones set in your
Keycloak admin console for the user in question.

## Requirements

Programs you will need include [cURL][curl] for actually sending the requests
via a shell or a command line, and [jq] for actually processing the responses
to the requests. The addresses below assume that the Tomcat instance running
the Wallet is running at the address http://localhost:8080, whereas the
authentication server Keycloak should be running at
http://kubernetes.docker.internal:9000/ on a Mac and http://localhost:9000/ on
Linux.

[curl]: https://curl.se/
[jq]: https://stedolan.github.io/jq/


## Creating a wallet

No authentication or authorization is needed here:

	# Generate payload string

	wallet_creation_payload=$(
		printf \
			'{"userEmail": "%s", "userId": "%s"}' \
			"$EUROPASS_USER_EMAIL" \
			"$EUROPASS_USER_ID"
	)

	# Echo to make sure the string is sensible

	echo "\npayload: $wallet_creation_payload\n"

	# Generate a wallet with the data string

	curl \
		-X POST \
		-H "Content-Type: application/json" \
		-H "Accept: application/json" \
		-d $wallet_creation_payload \
		http://localhost:8080/europass2/edci-wallet/api/v1/wallets

**Note:** it is important to use the ID of the user whose wallet is being
created, *not* the username. The ID *has* to be known by Keycloak, or any
subsequent wallet operations related to the given e-mail will be prevented. If
that happens, manual modification of the ID in the EDCI Wallet database is
required.

## Retrieving access- and refresh tokens

These will be used for authenticating a user and authorizing access to
resources. The commandsa are as follows:

	# Get tokens

	response=$(curl \
		-X POST \
		-s \
		-v \
		-d 'client_id=edci-wallet' \
		-d "username=$EUROPASS_USERNAME" \
		-d "password=$EUROPASS_PASSWORD" \
		-d 'grant_type=password' \
		-d "client_secret=$EUROPASS_WALLET_SECRET" \
		'http://kubernetes.docker.internal:9000/auth/realms/edci/protocol/openid-connect/token' \
	)

	# Extract them with jq

	access_token=$(jq -r '.access_token' <(echo $response))
	refresh_token=$(jq -r '.refresh_token' <(echo $response))
	token_type=$(jq -r '.token_type' <(echo $response))

	# Echo them to make sure sensible data came out

	echo "\naccess token: $access_token\n"
	echo "\nrefresh token: $refresh_token\n"
	echo "\ntoken type: $token_type\n"

The tokens and and their typess are included in the variables `access_token`,
`refresh_token` and `token_type`. These will need to be interpolated into the
below wallet access requests.


## Get wallet information

A GET request to a wallet address (identified by a user ID in the path) will
retrieve the wallet information:

	# Send wallet info request

	wallet_info=$(curl \
		-X GET \
		-s -v \
		-H "Accept: application/json" \
		-H "Authorization: $token_type $access_token" \
		-u "$EUROPASS_USERNAME:$EUROPASS_PASSWORD" \
		"http://localhost:8080/europass2/edci-wallet/api/v1/wallets/$EUROPASS_USER_ID" \
	)

	echo $wallet_info | jq


## Modifying a wallet

The data to be modified is to be sent in the payload or `-d` field of the PUT
request:

	curl \
		-X PUT \
		-s -v \
		-H "Accept: application/json" \
		-H "Authorization: $token_type $access_token" \
		-d "$payload_in_json" \
		"http://localhost:8080/europass2/edci-wallet/api/v1/wallets/$EUROPASS_USER_ID"


## Deleting a wallet

Simply send a DELETE request to the wallet address, along with an access
token:

	curl \
		-X DELETE \
		-s -v \
		-H "Accept: application/json" \
		-H "Authorization: $token_type $access_token" \
		"http://localhost:8080/europass2/edci-wallet/api/v1/wallets/$EUROPASS_USER_ID"



## Creating credentials from XML files

A credential is stored into a wallet with a suitable POST request. Note that
the `test_cert,xml` file refers to [the one in this
repository](./test_cert.xml):

	credential_creation_response=$(
		curl \
			-X POST \
			-s -v \
			-H "Content-Type: multipart/form-data" \
			-H "Accept: application/json" \
			-H "Authorization: $token_type $access_token" \
			-F '_credentialXML=@test_cert.xml' \
			"http://localhost:8080/europass2/edci-wallet/api/v1/wallets/$EUROPASS_USER_ID/credentials"
	)

	printf '%s\n' "$credential_creation_response" | jq

We use `printf` here to ignore control characters in the response JSON.

## Requesting credentials from the Wallet

	credential_fetch_response=$(
		curl \
			-X GET \
			-s -v \
			-H "Accept: application/json" \
			-H "Authorization: $token_type $access_token" \
			"http://localhost:8080/europass2/edci-wallet/api/v1/wallets/$EUROPASS_USER_ID/credentials"
	)

	printf '%s\n' "$credential_fetch_response"| jq

Same as above, `printf` is used to ignore control characters in the response
JSON, as `jq` does not appreciate them.

## Bonus: request for EDCI Keycloak endpoints

This will return all possible Keycloak endpoints. Not needed in communicating
with the Wallet, but might come in handy if and endpoint name is forgotten:

	endpoints=$(curl \
		-X GET \
		"http://localhost:9000/auth/realms/edci/.well-known/openid-configuration" \
	)

	echo $endpoints | jq

