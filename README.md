# Europass documentation and status

Contains documentation and status updates on Europass and its modifications.
More specifically the files are as follows.

## [setup.md](./setup.md)

Contains information on how to set up a running intance of the EDCI Wallet and
other EDCI components. This is a rather involved process, and Europass version
≥ 1.4 should probably be used if the instructions are to work at all.

## [wallet_requests.md](./wallet_requests.md)

Contains readily crafted cURL commands for communicating with the EDCI Wallet.
A more sophisticated (read automated) means of communication might be needed
outside of testing individual wallet endpoints, but the required information
can be retrieved from this file. The most up-to-date information can always be
retrieved from the Wallet Swagger-UI JSON file generated during its
compilation. One version of it is listed in this repository.

## [apis/](./apis)

Documents the APIs of the EDCI Wallet and Issuer v1.4.1. The files in here
contain the URLs of the different endpoints of the applications, the types of
requests they allow and what data needs to be passed to the endpoints for them
to function.

## [mock-credentials/](./mock-credentials)

Test certificates in XML form. These can be fed to the Europass Wallet with a
suitable HTTP request.

## [creating_self_signed_certificates.md](./creating_self_signed_certificates.md)

Contains instructions on how to create self-signed certificates with OpenSSL.
These are needed when issuing test credentials, as a certificate needs to be
sealed with a digital signature, whether it is used for testing or not. The
actual e-seals are not available for testing, so this is the next best
solution.
