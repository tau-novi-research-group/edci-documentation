# Create databases
CREATE DATABASE IF NOT EXISTS wallet_db;
CREATE DATABASE IF NOT EXISTS issuer_db;

# Create root user and grant rights
CREATE USER 'edci'@'localhost' IDENTIFIED BY 'password';
GRANT ALL ON wallet_db.* TO 'edci'@'%';
GRANT ALL ON issuer_db.* TO 'edci'@'%';
