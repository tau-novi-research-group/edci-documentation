# EDCI database initialization files

This directory should be mounted into the `edci-mysql` Docker container during
its creation, as the folder `/docker-entrypoint-initdb.d`. Any `.sh`, `.sql`
and `.sql.gz` files in this folder should then be scanned and ran by the
container automatically, when it is first created.
