# Creating self-signed certificates for issuing Europass credentials

In order to issue Europass-compliant digital credentials, the credentials need
to be sealed with digital signatures. As the e-seals used to officially issue
credentials are carefully guarded assets and thus unavailable for testing
purposes, Europass developers need to use so called mock seals to sign any
test credentials.

Europass themselves recommend the use of third party websites for the creation
of the mock seals or self-signed certificates, and [NexU] for signing them. As
of 2021-12, the NexU distribution available via the NexU website seems to have
problems working with the EDCI Issuer, but apparently the EDCI Issuer will
have its own signing capabilites in its future release.

[NexU]: https://github.com/nowina-solutions/nexu

Even if NexU has trouble functioning for now, we can still avoid the use of
3rd party websites in the creation of the certificates themselves by using the
command-line tool [OpenSSL] to both create and self-sign mock seals for
testing. What follows is a brief tutorial on how the program is used to create
and sign the test credentials.

[OpenSSL]:https://www.openssl.org/

## Installing OpenSSL

This is best achieved via a package manager of your choice. On newer MacOS
and [HomeBrew] versions, running

	brew update
	brew install openssl

should be enough. Newer versions of HomeBrew will automatically add the tool
to the PATH variable of your system and source it to make it available without
restarting your shell. If this does not happen, manually linking to the binary
in [Z Shell][zsh] (`zsh`) with

	echo 'export PATH="/path/to/openssl/bin:$PATH"' >> ~/.zshrc
	source ~/.zshrc

should do. This can be adapted for other shells such Bash and other package
managers such as Apt on Debian-based Linux distributions.

[HomeBrew]: https://brew.sh/
[zsh]: https://www.zsh.org/


## Generating a self-signed certificate with OpenSSL

A self-signed certificate is simply a certificate that has been signed with
its own private key. Therefore the private key `domain.key` must be generated
first:

	openssl genrsa -des3 -out domain.key 2048

Here the `genrsa` sub-command generates the 2 MiB long private key. The
`-des3` flag may be left out, if no encryption of the key is needed in
testing. The `domain` prefix of the key file may be changed to something more
descriptive, or to match the domain that the key will be used in.

After the private key has bee generated, it can be used to sign certificates.
The SSL protocol requires that a [Certificate Signing Request][CSR] (CSR) be
made for the signing process to start. We will then generate such a CSR file
`domain.csr` with our private `domain.key`:

	openssl req -key domain.key -new -out domain.csr

[CSR]: https://en.wikipedia.org/wiki/Certificate_signing_request

This will result in the following prompt being displayed:

	Enter pass phrase for domain.key:
	You are about to be asked to enter information that will be incorporated
	into your certificate request.
	What you are about to enter is what is called a Distinguished Name or a DN.
	There are quite a few fields but you can leave some blank
	For some fields there will be a default value,
	If you enter '.', the field will be left blank.
	-----
	Country Name (2 letter code) []:FI
	State or Province Name (full name) []:Pirkanmaa
	Locality Name (eg, city) []:Tampere
	Organization Name (eg, company) []:Tampere University
	Organizational Unit Name (eg, section) []:NOVI research group
	Common Name (eg, fully qualified host name) []:domain
	Email Address []:your.email@tuni.fi

	Please enter the following 'extra' attributes
	to be sent with your certificate request
	A challenge password []:.
	An optional company name []:.

Note that the field `Common Name` *must* match the Fully Qualified Domain Name
(FQDN) of the domain the CSR will be used in. On a Mac, this can be found
under *System preferences → Sharing → Computer Name * DNS Suffix*, where the
symbol *\** denotes concatenation, and *DNS Suffix* can be found in *System
Preferences → Networks → Ethernet → Advanced… → DNS*.

**Note:** in a local test environment, the `Common Name` should probably be
`localhost` or your computer name. In other words, the *DNS suffix* can be
left out if no public server is being secured.

Now that we have a certificate signing request up and running, we can actually
create a self-signed credential `domain.crt` or `domain.pem`:

	openssl x509 \
		-signkey domain.key \
		-in domain.csr \
		-req \
		-days 365 \
		-out domain.pem

Here the `-days <integer>` denotes how many days the certificate will be
valid, starting from the creation date.

Finally, Europass uses the [PKCS #12] credential archival format for handling
credentials and their metadata. The above certificate can be transformed to
this format with the command

	openssl pkcs12 \
		-export \
		-in domain.pem \
		-inkey domain.key \
		-out domain.pfx \

This should be usable as a self-signed credential by the EDCI Issuer.

[PKCS #12]: https://en.wikipedia.org/wiki/PKCS_12

## How to use the self-signed credential

When self-signing credentials with an EDCI Issuer installed locally, the path
to the credential file should be specified in the file tomcat → conf → edci →
issuer → issuer.properties. The property

	dss.cert.path=/path/to/credential.pfx

in said file determines which file on the host system is to be used as the
credential. Once this has been set to the appropriate value, issuing
credentials via the Issuer should work.

**Note:** EDCI issuer v1.4 does not yet support self-signed credentials. When
this ability will be included is a mystery, but apparently it is being tested
as of 2021-12-20.


## Conclusion

That should be it. The file `domain.pfx` should be usable as a self-signed
certificate or a mock seal in signing Europass credentials. Warnings about the
certificate being untrusted might arise, but those should be ignorable in a
test environment.

If required, OpenSSL might also be used to create our own Certificate
Authority- or CA-signed credentials. In short, the process is very similar to
creating a self-signed certificate. The only difference is that in the
CSR-creation phase, a configuration file `domain.ext` with the contents

	authorityKeyIdentifier=keyid,issuer
	basicConstraints=CA:FALSE
	subjectAltName = @alt_names
	[alt_names]
	DNS.1 = domain

where `DNS.1` is again the domain of our "website" must be provided via the
flag `-extfile domain.ext`. For further instructions, see
<https://www.baeldung.com/openssl-self-signed-cert#creating-a-ca-signed-certificate-with-our-own-ca>.
